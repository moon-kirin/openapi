openapi: 3.0.0
info:
  title: Kirin
  version: '1.2'
  contact:
    name: Rei Izumi
    email: rei.izumi@moon.cat
  license:
    name: MIT
    url: 'https://gitlab.com/moon-kirin/service/-/raw/master/LICENSE?ref_type=heads'
  description: |-
    This service allows you to define the monthly and extra income during the year to organize them in different fixed costs and budgets. For each budget you can add purchases made.
    A Payroll must be previously defined to be able to create new objects associated with that year, if not, they will return a conflict error.

    This application does not activate alerts (for example when exceeding the limit of a budget), it offers the ability to organize the data and the user is free to exceed or adjust as they consider. 

    The data entered is assigned to the user identifier, so no other user can access.

    Only production servers are published to the internet.
servers:
  - url: 'http://localhost:8080'
    description: Local
  - url: 'https://kirin-extranet.test.moon.intranet'
    description: Test
  - url: 'https://kirin.moon.cat'
    description: Production
paths:
  /api/v1/payrolls:
    get:
      summary: List of years
      tags:
        - payroll
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Year'
      operationId: get-v1-payrolls
      description: Returns the list of all years that have been configured ordered from the oldest to newest
    parameters: []
  '/api/v1/payrolls/{year}':
    parameters:
      - schema:
          type: integer
        name: year
        in: path
        required: true
    get:
      summary: Retrieve payroll
      tags:
        - payroll
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Payroll'
        '404':
          description: Not Found
      operationId: get-v1-payrolls-year
      description: Retrieve the payroll for the selected year
    post:
      summary: Create payroll
      operationId: post-v1-payrolls-year
      responses:
        '201':
          description: Created
          headers:
            Location:
              schema:
                type: string
              description: URL to retrieve the payroll
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequest'
        '409':
          description: Conflict
          headers:
            Location:
              schema:
                type: string
              description: URL to retrieve the payroll
      description: Create a new payroll if it does not exist
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Payroll'
      tags:
        - payroll
    put:
      summary: Update payroll
      operationId: put-v1-payrolls-year
      responses:
        '204':
          description: No Content
          headers:
            Location:
              schema:
                type: string
              description: URL to retrieve the payroll
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequest'
        '404':
          description: Not Found
      description: Update an existing payroll
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Payroll'
        description: ''
      tags:
        - payroll
    delete:
      summary: Delete payroll
      operationId: delete-v1-payrolls-year
      responses:
        '204':
          description: No Content
        '404':
          description: Not Found
      description: Delete an existing payroll
      tags:
        - payroll
  /api/v1/budgets:
    get:
      summary: Retrieve budgets
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Budget'
      operationId: get-v1-budgets
      description: Retrieve all budgets for the indicated year
      tags:
        - budgets
      parameters:
        - $ref: '#/components/parameters/YearParameter'
    parameters: []
    post:
      summary: Create budget
      operationId: post-v1-budgets
      responses:
        '201':
          description: Created
          headers:
            Location:
              schema:
                type: string
              description: URL to retrieve the budget
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequest'
        '409':
          description: Payroll for the selected year does not exist
      description: Create a new budget
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Budget'
      tags:
        - budgets
  '/api/v1/budgets/{id}':
    parameters:
      - schema:
          type: integer
        name: id
        in: path
        required: true
    get:
      summary: Retrieve budget
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Budget'
        '404':
          description: Not Found
      operationId: get-v1-budgets-id
      description: Retrieve an specific budget by id
      tags:
        - budgets
    put:
      summary: Update budget
      operationId: put-v1-budgets-id
      responses:
        '204':
          description: No Content
          headers:
            Location:
              schema:
                type: string
              description: URL to retrieve the budget
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequest'
        '404':
          description: Not Found
      description: |-
        Update an specific budget by id.
        In case the value of year is different, it will be discarded and the original value will be used.
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Budget'
      tags:
        - budgets
    delete:
      summary: Delete budget
      operationId: delete-v1-budgets-id
      responses:
        '204':
          description: No Content
        '404':
          description: Not Found
      description: Delete an specific budget by id
      tags:
        - budgets
  /api/v1/savings:
    get:
      summary: Retrieve savings
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Saving'
      operationId: get-v1-savings
      description: Retrieve all savings for the indicated year
      parameters:
        - $ref: '#/components/parameters/YearParameter'
      tags:
        - saving
    parameters: []
    post:
      summary: Create saving
      operationId: post-v1-savings
      responses:
        '201':
          description: Created
          headers:
            Location:
              schema:
                type: string
              description: URL to retrieve the saving
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequest'
        '409':
          description: Payroll for the selected year does not exist
      description: Create a new saving
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Saving'
      tags:
        - saving
  '/api/v1/savings/{id}':
    parameters:
      - schema:
          type: integer
        name: id
        in: path
        required: true
    get:
      summary: Retrieve saving
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Saving'
        '404':
          description: Not Found
      operationId: get-v1-savings-id
      description: Retrieve an specific saving by id
      tags:
        - saving
    put:
      summary: Update saving
      operationId: put-v1-savings-id
      responses:
        '204':
          description: No Content
          headers:
            Location:
              schema:
                type: string
              description: URL to retrieve the saving
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequest'
        '404':
          description: Not Found
      description: |-
        Update an specific saving by id.
        In case the value of year is different, it will be discarded and the original value will be used.
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Saving'
      tags:
        - saving
    delete:
      summary: Delete saving
      operationId: delete-v1-savings-id
      responses:
        '204':
          description: No Content
        '404':
          description: Not Found
      description: Delete an specific saving by id
      tags:
        - saving
  /api/v1/fixed-expenses:
    get:
      summary: Retrieve fixed expenses
      tags:
        - fixed expenses
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/FixedExpense'
      operationId: get-v1-fixed-expenses
      description: Retrieve all fixed expenses for the indicated year
      parameters:
        - $ref: '#/components/parameters/YearParameter'
    parameters: []
    post:
      summary: Create fixed expense
      operationId: post-v1-fixed-expenses
      responses:
        '201':
          description: Created
          headers:
            Location:
              schema:
                type: string
              description: URL to retrieve the fixed expense
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequest'
        '409':
          description: Payroll for the selected year does not exist
      description: Create a new fixed expense
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/FixedExpense'
      tags:
        - fixed expenses
  '/api/v1/fixed-expenses/{id}':
    parameters:
      - schema:
          type: integer
        name: id
        in: path
        required: true
    get:
      summary: Retrieve fixed expense
      tags:
        - fixed expenses
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/FixedExpense'
        '404':
          description: Not Found
      operationId: get-v1-fixed-expenses-id
      description: Retrieve an specific fixed expense by id
    put:
      summary: Update fixed expense
      operationId: put-v1-fixed-expenses-id
      responses:
        '204':
          description: No Content
          headers:
            Location:
              schema:
                type: string
              description: URL to retrieve the fixed expense
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequest'
        '404':
          description: Not Found
      description: |-
        Update an specific fixed expense by id.
        In case the value of year is different, it will be discarded and the original value will be used.
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/FixedExpense'
      tags:
        - fixed expenses
    delete:
      summary: Delete fixed expense
      operationId: delete-v1-fixed-expenses-id
      responses:
        '204':
          description: No Content
        '404':
          description: Not Found
      description: Delete an specific fixed expense by id
      tags:
        - fixed expenses
  /api/v1/purchases:
    get:
      summary: Retrieve purchases
      tags:
        - purchase
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Purchase'
      operationId: get-v1-purchases
      description: Retrieve all purchases according to a budget id
      parameters:
        - schema:
            type: integer
          in: query
          name: budgetId
          description: Budget Id
          required: true
    post:
      summary: Create new purchase
      operationId: post-v1-purchases
      responses:
        '201':
          description: Created
          headers:
            Location:
              schema:
                type: string
              description: URL to retrieve the purchase
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequest'
        '409':
          description: BudgetId does not exist
      description: Create a new purchase
      tags:
        - purchase
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Purchase'
    parameters: []
  '/api/v1/purchases/{id}':
    parameters:
      - schema:
          type: integer
        name: id
        in: path
        required: true
    get:
      summary: Retrieve purchase
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Purchase'
        '404':
          description: Not Found
      operationId: get-v1-purchases-id
      description: Retrieve an specific purchase by id
      tags:
        - purchase
    put:
      summary: Update purchase
      operationId: put-v1-purchases-id
      responses:
        '204':
          description: No Content
          headers:
            Location:
              schema:
                type: string
              description: URL to retrieve the purchase
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequest'
        '404':
          description: Not Found
      description: Update an specific purchase by id
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Purchase'
      tags:
        - purchase
    delete:
      summary: Delete purchase
      operationId: delete-v1-purchases-id
      responses:
        '204':
          description: No Content
        '404':
          description: Not Found
      description: Delete an specific purchase by id
      tags:
        - purchase
  /api/v1/categories:
    get:
      summary: Retrieve all categories
      tags:
        - categories
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Category'
      operationId: get-api-v1-categories
      description: Retrieve all categories in the system
components:
  schemas:
    Payroll:
      title: Payroll
      type: object
      description: Definition of income and types of budgets
      properties:
        year:
          type: integer
          example: 2022
          description: Year defined for income. Must be unique
          readOnly: true
        payroll01:
          example: 1000
          description: January payroll
          type: number
        payroll02:
          example: 1000
          description: February payroll
          type: number
        payroll03:
          example: 1000
          description: March payroll
          type: number
        payroll04:
          type: number
          example: 1000
          description: April payroll
        payroll05:
          type: number
          example: 1000
          description: May payroll
        payroll06:
          type: number
          example: 1000
          description: June payroll
        payroll07:
          type: number
          example: 1000
          description: July payroll
        payroll08:
          type: number
          example: 1000
          description: August payroll
        payroll09:
          type: number
          example: 1000
          description: September payroll
        payroll10:
          type: number
          example: 1000
          description: October payroll
        payroll11:
          type: number
          example: 1000
          description: November payroll
        payroll12:
          type: number
          example: 1000
          description: December payroll
        extra:
          type: array
          description: Extra income that are not associated with a month
          items:
            $ref: '#/components/schemas/PayrollExtra'
      required:
        - payroll01
        - payroll02
        - payroll03
        - payroll04
        - payroll05
        - payroll06
        - payroll07
        - payroll08
        - payroll09
        - payroll10
        - payroll11
        - payroll12
    PayrollExtra:
      title: PayrollExtra
      type: object
      description: Income not assigned to a month or requiring a name to explain it
      properties:
        name:
          type: string
          example: Christmas
          maxLength: 50
          description: Name to describe the income
        payroll:
          type: number
          example: 200
          description: Amount
      required:
        - name
        - payroll
    Saving:
      title: Saving
      type: object
      description: Total to save in the year for a specific thing
      properties:
        id:
          type: integer
          description: Identifier
          example: 83
          readOnly: true
        year:
          type: integer
          example: 2022
          description: |-
            Year assigned. There must be a Payroll associated with the year.
            Once it has been created, it cannot be modified.
        name:
          type: string
          description: Name to describe the saving
          example: New laptop
          maxLength: 50
        amount:
          type: number
          description: 'Total saved per year '
          example: 250
      required:
        - year
        - name
        - amount
    Budget:
      title: Budget
      type: object
      description: 'Budget to which purchases will be assigned '
      properties:
        id:
          type: integer
          example: 4
          description: Identifier
          readOnly: true
        year:
          type: integer
          example: 2022
          description: |-
            Year assigned. There must be a Payroll associated with the year.
            Once it has been created, it cannot be modified.
        name:
          type: string
          maxLength: 50
          example: Home
          description: 'Name to describe the budget '
        budget:
          type: number
          example: 200
          description: Amount
        type:
          type: string
          description: The budget can be monthly or extra of a single amount
          enum:
            - MONTHLY
            - SINGLE
      required:
        - year
        - name
        - budget
        - type
    FixedExpense:
      title: FixedExpenses
      type: object
      description: 'Fixed expenses that are allocated during the year '
      properties:
        id:
          type: integer
          example: 5
          description: Identifier
          readOnly: true
        year:
          type: integer
          example: 2022
          description: |-
            Year assigned. There must be a Payroll associated with the year.
            Once it has been created, it cannot be modified.
        name:
          type: string
          example: Netflix
          maxLength: 50
          description: Name to describe the fixed expense
        description:
          type: string
          example: Netflix monthly subscription
          maxLength: 100
          description: Description to give more details about this expense
        categoryId:
          type: integer
          example: 5
          description: 'Each expense is assigned to a category, each category has an assigned id'
        type:
          type: string
          enum:
            - UNITARY
            - MONTHLY
            - ANNUAL
          description: |-
            A fixed cost can be:
            - unitary: it is paid once in a specific month.
            - monthly: the payment is assigned to more than 1 month.
            - annual: it is annual and does not depend on a specific month.
        price:
          type: number
          example: 12.5
          description: Amount
        requireMark:
          type: boolean
          description: 'Indicates if the system should ask if it has been paid. If it is monthly, the user must indicate it for each month individually'
        months:
          type: array
          description: 'List of months defined for the expense. The ''annual'' type must be defined for month 0 '
          items:
            $ref: '#/components/schemas/FixedExpenseMonth'
      required:
        - year
        - name
        - categoryId
        - type
        - price
    FixedExpenseMonth:
      title: FixedExpensesMonth
      type: object
      description: 'List of months in which the expense affects and if they have been paid '
      properties:
        month:
          type: integer
          example: 11
          description: Month in which the payment is made . 0 for 'annual' type
          minimum: 0
          maximum: 12
        paid:
          type: boolean
          description: 'Indicates if the payment has been made that month '
      required:
        - month
        - paid
    Purchase:
      title: Purchase
      type: object
      description: 'Purchase assigned to a budget '
      properties:
        id:
          type: integer
          example: 7
          description: Identifier
          readOnly: true
        budgetId:
          type: integer
          example: 36
          description: Budget Identifier
        name:
          type: string
          example: Cup of coffee
          maxLength: 50
          description: Name to describe the purchase
        description:
          type: string
          example: Coffee from the corner bar to recharge the batteries
          maxLength: 100
          description: Description to give more details about this expense
        categoryId:
          type: integer
          example: 11
          description: 'Each purchase is assigned to a category, each category has an assigned id '
        price:
          type: number
          example: 1.23
          description: Amount
        month:
          type: integer
          minimum: 0
          maximum: 12
          example: 3
          description: |-
            Month to assign the purchase.
            For budgets with 'single' type, the value must be 0
      required:
        - budgetId
        - name
        - categoryId
        - price
        - month
    BadRequest:
      description: ''
      type: object
      x-examples: {}
      properties:
        classViolations:
          type: array
          items:
            type: object
        parameterViolations:
          type: array
          uniqueItems: true
          minItems: 1
          items:
            type: object
            properties:
              constraintType:
                type: string
                minLength: 1
                example: PARAMETER
              message:
                type: string
                minLength: 1
                example: Size must be between 0 and 50
              path:
                type: string
                minLength: 1
                example: postV1FixedExpenses.fixedExpense.name
              value:
                type: string
                minLength: 1
            required:
              - constraintType
              - message
              - path
              - value
        propertyViolations:
          type: array
          items:
            type: object
        returnValueViolations:
          type: array
          items:
            type: object
      required:
        - classViolations
        - parameterViolations
        - propertyViolations
        - returnValueViolations
    Category:
      type: object
      title: Category
      description: 'Category to classify the type of expenses '
      properties:
        id:
          type: integer
          description: Identifier
          example: 24
          readOnly: true
        name:
          type: string
          maxLength: 100
          example: House
          description: Category name
          readOnly: true
        description:
          type: string
          maxLength: 1000
          example: Household expenses
          description: Description of the type of expenses that should be indicated in this category
          readOnly: true
        icon:
          type: string
          example: base64(image)
          description: 'Icon to display. Transformed into base64 '
          readOnly: true
      readOnly: true
    Year:
      type: object
      title: Year
      description: The Payroll year is the identifier that all other objects use
      additionalProperties: false
      properties:
        year:
          type: integer
          description: Payroll year
          example: 2022
      readOnly: true
  securitySchemes:
    test:
      type: oauth2
      flows:
        authorizationCode:
          authorizationUrl: 'https://keycloak.test.moon.intranet/auth/realms/Moon/protocol/openid-connect/auth'
          tokenUrl: 'https://keycloak.test.moon.intranet/auth/realms/Moon/protocol/openid-connect/token'
          scopes:
            'kirin:user': User
      description: Test environment NOT published to internet
    production:
      type: oauth2
      flows:
        authorizationCode:
          authorizationUrl: 'https://auth.moon.cat/auth/realms/Moon/protocol/openid-connect/auth'
          tokenUrl: 'https://auth.moon.cat/auth/realms/Moon/protocol/openid-connect/token'
          refreshUrl: ''
          scopes:
            'kirin:user': User
      description: Production environment
  parameters:
    YearParameter:
      name: year
      in: query
      schema:
        type: integer
        example: 2022
      description: Year to filter
      required: true
  responses: {}
tags:
  - name: budgets
  - name: categories
  - name: fixed expenses
  - name: payroll
  - name: purchase
  - name: saving
security:
  - test:
      - 'kirin:user'
  - production:
      - 'kirin:user'
